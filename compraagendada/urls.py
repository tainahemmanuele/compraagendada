"""compraagendada URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url

from app.views.AdmView import *
from app.views.ClientView import *
from app.views.HomeView import *
from app.views.LoginView import *
from app.views.ProductView import *
from app.views.PainelView import PainelView
from app.views.BasketView import *
from app.views.ShowcaseView import *
from app.views.CartView import *
from app.views.PurchaseView import *
from app.views.SocialView import *

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^facebook/$', FacebookView.as_view(), name='facebook'),
    url(r'^twitter/$', TwitterView.as_view(), name='twitter'),
    url(r'^about/$', AboutView.as_view(), name='about'),
    url(r'^register_client/$', RegisterClientView.as_view(), name='register_client'),
    url(r'^register_adm/$', RegisterAdmView.as_view(), name='register_adm'),
    url(r'^product/(?P<product_id>\d+)/$', ProductClientView.product_detail_client, name='product_client'),
    url(r'^basket/(?P<basket_id>\d+)/$', BasketView.basket_detail, name='basket_client'),
    url(r'^showcase_product/$', ShowcaseViewProduct.as_view(), name='showcase_products'),
    url(r'^showcase_basket/$', ShowcaseViewBasket.as_view(), name='showcase_baskets'),
    url(r'^login/$', LoginView.as_view(), name='login'),
    url(r'^account/$', DetailClientView.as_view(), name='detail'),
    url(r'^account/logout/$', LogoutView.as_view(), name='logout'),
    url(r'^account/edit_client/$', EditClientView.as_view(), name='edit_client'),
    url(r'^account/change_password_client/$', ChangePasswordClientView.as_view(), name='change_password_client'),
    url(r'^account/delete_client/$', delete_client, name='delete_client'),
    url(r'^account/purchases_historic/$', MyPurchaseListView.as_view(), name='purchases_historic'),
    url(r'^account/purchases_historic/purchase_(?P<pk>\d+)/$', DetailMyPurchaseView.as_view(), name='purchase'),
    url(r'^my_cart/$', EditCartView.as_view(), name='my_cart'),
    url(r'^my_cart/confirm_purchase/$', ShowCartView.as_view(), name='confirm_purchase'),
    url(r'^my_cart/confirm_purchase/buying/$', buying_view, name='buying'),
    url(r'^add_product/(?P<pk>\d+)/$', add_Product, name='add_product'),
    url(r'^add_basket/(?P<pk>\d+)/$', add_Basket, name='add_basket'),

    url(r'^painel/$', PainelView.as_view(), name='painel'),
    url(r'^painel/account/$', DetailAdmView.as_view(), name='detail_adm'),
    url(r'^painel/account/edit/$', EditAdmView.as_view(), name='edit_adm'),
    url(r'^painel/account/change_password_adm/$', ChangePasswordAdmView.as_view(), name='change_password_adm'),
    url(r'^painel/account/delete_adm/$', delete_adm, name='delete_adm'),
    url(r'painel/create_basketskel/$', CreateBasketSkelView.as_view(), name='create_basketskel'),
    url(r'painel/create_basket/$', CreateBasketView.as_view(), name='create_basket'),
    url(r'painel/auto_create_basket/$', AutoCreateBasketView.as_view(), name='auto_create_basket'),
    url(r'painel/basket_list/$', BasketListView.as_view(), name='basket_list'),
    url(r'painel/basketskel_list/$', BasketSkelListView.as_view(), name='basketskel_list'),
    url(r'painel/basket_list/(?P<basket_id>\d+)/$', BasketDetailView.basket_detail, name='basket_list_detail'),
    url(r'^painel/basket_list/edit_basket/(?P<pk>[0-9]+)/$', EditBasketView.as_view(), name='edit_basket'),
    url(r'^painel/new_product_type/$', NewProductTypeView.as_view(), name='new_product_type'),
    url(r'^painel/product_type_list/$', ProductTypeListView.as_view(), name='product_type_list'),
    url(r'^painel/delete_product_type/(?P<pk>\d+)/$', delete_product_type, name='delete_product_type'),
    url(r'^painel/register_product/$', RegisterProductView.as_view(), name='register_product'),
    url(r'^painel/product/(?P<product_id>\d+)/$',ProductView.product_detail, name='product'),
    url(r'^painel/product/edit_product/(?P<pk>[0-9]+)/$', EditProductView.as_view(), name='edit_product'),
    url(r'^painel/products/$', ProductListView.as_view(), name ='products'),
    url(r'^painel/products/removed/$', RemovedProductListView.as_view(), name ='removed_products'),
    url(r'^painel/delete_product/(?P<pk>[0-9]+)/$', delete_product, name ='delete_product'),
    url(r'^painel/delete_basket/(?P<pk>[0-9]+)/$', delete_basket, name ='delete_basket'),
    url(r'^painel/delete_basketskel/(?P<pk>[0-9]+)/$', delete_basketskel, name ='delete_basketskel'),
    url(r'^painel/register_purchases/$', RegisterPurchaseView.as_view(), name='register_purchase'),
    url(r'^painel/purchases/$', PurchaseListView.as_view(), name='list_purchases'),
    url(r'^painel/purchases/(?P<pk>[0-9]+)/$', EditPurchaseView.as_view(), name='purchase_cotrol'),



]

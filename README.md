**Compra Agendada**
===================

Repositório referente a aplicação web Compra Agendada - Projeto da disciplina de Projeto 1 - UFCG - 2018.2

----------
Sobre o Compra Agendada
-------------
O Compra Agendada é um serviço de venda de cestas básicas a preços mais acessíveis às populações mais vulneráveis, usando um software para selecionar os produtos que irão compor as cestas básicas da semana de forma a minimizar seus custos para os clientes. Através de uma simples mensagem via whatsapp, o cliente pode agendar a entrega de um dos tipos de cestas disponíveis e aguardar a entrega da mesma em local combinado. 
O Compra Agendada também permite que  o usuário possa fazer o pedido de uma cesta ou de produtos avulsos via aplicação web, para isto, basta o cliente fazer um cadastro prévio na aplicação.


Requerimentos para executar o Compra Agendada
-------------
> **Passos**

> - Clone o projeto (necessário ter o pip instalado na maquina, python 3.5.6):

        git clone https://gitlab.com/tainahemmanuele/compraagendada.git

> - Após clonar, entre na pasta do projeto , e execute:

        pip install -r requirements.txt

Executando o projeto
-------------
> **Execute no terminal:**

         python manage.py makemigrations
         python manage.py migrate
         python manage.py runserver

> - Por fim,acesse: http://localhost:8000/

Regras para implementação (commit, push, pull)
-------------

> **Criando a branch para desenvolver:**

> - Acesse o tuleap-campus, e observe o número da sua task (ex. Task #19152)

> - Crie uma nova branch com nome task_numero_da_task (sem hashtag, tudo minusculo):

      git checkout -b <nome_da_branch>

> - Nessa sua branch faça seus commits e aletrações necessárias.

> **Enviando suas alterações para o git:**
> - No terminal, execute:

        git add .

        git commit -am "Mensagem do commit"

        git checkout master

        git pull origin master

        git checkout <nome_da_sua_branch>

        git merge master

        git push origin <nome_da_sua_branch>

Vá para o gitlab , clique na branch corresponde as alterações enviadas e solicite o merge request.

Usando CI 
-------------
O projeto usa o gitlab CI com pipelines que verificam a integridade do código em duas situações:
> - Antes de fazer merge com a master
> - Antes de fazer deploy com o heroku (esta etapa só é executada após a autorização de merge com master)

Caso haja algum problema em alguma dessas etapas, o merge e o deploy não serão feitos. Se isto ocorrer por motivos de problemas no gitlab runner (onde executam as nossas pipelines), basta solicitar a execução das pipelines novamente. Para isso, siga os passos:

> - Na página do repositório, clique em ***CI/CD*** e selecione ***Pipelines***

![](https://preview.ibb.co/dG0or9/image1-gitlab.png)

> - Depois clique em ***Run Pipeline*** para executar as pipelines novamente

![](https://preview.ibb.co/cmbmB9/image2-gitlab.png)


Docker do Compra Agendada
-------------
> - O container do Compra Agendada funciona como o build da aplicação de forma mais rápida. Esse container pode ser usado tanto em produção (com CI) como localmente para o desenvolvedor testar suas alterações

> **Executando**
> - Para criar e executar o container localmente:

        docker-compose up -d --build

> - Esse container executa os mesmos comandos para a execução de código usada no TERMINAL

> - Usando container, o link para acesso a aplicação é : [http://localhost:8008](http://localhost:8008)

> **Atualizei meu código e o container continua com as alterações anteriores. O que fazer?**
> 
> - Localmente, toda alteração que for feita , o docker não irá atualizar, pois para isso é necessário que a imagem docker
do Compra Agendada seja recontruida. Para isso , faça os seguintes passos para atualizar a imagem localmente com as alterações:

        docker stop compraagendada
        docker rm  compraagendada
        docker-compose up -d --build 

Versão em produção do Compra Agendada:
-------------

Versão mais estável do sistema:

[http://compraagendada.herokuapp.com/](http://compraagendada.herokuapp.com/)
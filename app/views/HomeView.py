from django.shortcuts import render
from django.views.generic import *
from app.models import  Product, Basket, BasketSkel

class HomeView(ListView):
    def get(self, request):
        products = Product.objects.all().order_by('?')
        baskets = Basket.objects.order_by('?')
        baskets2= BasketSkel.objects.all()
        return render(request, 'index.html', {'products': products, 'baskets':baskets})

class AboutView(TemplateView):
    template_name = "About.html"
#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
from django.contrib.auth.mixins import UserPassesTestMixin
from django.views.generic import *
from chartjs.views.lines import BaseLineChartView
from app.models import Product, ProductType
from django.views.generic import TemplateView
from django.db.models import Count, Q
from django.shortcuts import render


class PainelView(UserPassesTestMixin, TemplateView):
    """
    Displays the login form.
    """
    login_url = '/login/'
    permission_denied_message = "Acesso ao painel negado."
    template_name = 'login-files/PainelHome.html'
    model = Product
    context_object_name = 'products'


    def test_func(self):
        if(hasattr(self.request.user, 'serviceadm')):
            return True
        else:
            return False

    def get(self,request):
        categories_mais = list()
        categories_menos = list()
        mais_vendidos = list()
        menos_vendidos = list()

        for product in Product.objects.all():
            print(product.sold)
            if product.sold >= 10:
                mais_vendidos.append(product.name)
                categories_mais.append(product.sold)

        for product in Product.objects.all():
            print(product.sold)
            if product.sold < 10:
                menos_vendidos.append(product.name)
                categories_menos.append(product.sold)

        return render(request, 'login-files/PainelHome.html', {
            'categories_mais': json.dumps(categories_mais),
            'mais_vendidos': json.dumps(mais_vendidos),
            'categories_menos': json.dumps(categories_menos),
            'menos_vendidos': json.dumps(menos_vendidos)
        })




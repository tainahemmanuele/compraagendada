from django.views.generic import RedirectView


class FacebookView(RedirectView):
    url = 'http://facebook.com/'


class TwitterView(RedirectView):
    url = 'http://twitter.com/'

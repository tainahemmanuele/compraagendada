from django.contrib.auth.mixins import UserPassesTestMixin
from django.contrib import messages
from django.contrib.auth.models import User
from django.views.generic import *
from django.core.exceptions import PermissionDenied
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.urls import reverse
from app.models import Purchase, Cart, ServiceClient, PurchaseBasket, PurchaseProduct
from app.forms import PurchaseForm, RegisterPurchaseForm, PurchaseBasketFormSet


class DetailMyPurchaseView(UserPassesTestMixin, DetailView) :
    login_url = "/login/"
    permission_denied_message = 'Acesso apenas para clientes.'
    template_name = 'MyPurchaseDetail.html'
    model = Purchase
    context_object_name = 'purchase'

    def get_queryset(self):
        qs = super(DetailMyPurchaseView, self).get_queryset().filter(client=self.request.user)
        return qs

    def test_func(self):
        if (hasattr(self.request.user, 'serviceclient')):
            return True
        else:
            return False


class EditPurchaseView(UserPassesTestMixin, UpdateView):
    login_url = '/login/'
    permission_denied_message = "Acesso ao painel negado."
    model = Purchase
    form_class = PurchaseForm
    template_name = 'login-files/EditPurchase.html'

    def test_func(self):
        if(hasattr(self.request.user, 'serviceadm')):
            return True
        else:
            return False

    def get_success_url(self):
        return reverse('list_purchases')


class PurchaseListView(UserPassesTestMixin, ListView):
    login_url = '/login/'
    permission_denied_message = "Acesso ao painel negado."
    model = Purchase
    context_object_name = 'purchases'
    template_name = 'login-files/PurchaseList.html'

    def test_func(self):
        if(hasattr(self.request.user, 'serviceadm')):
            return True
        else:
            return False

    def get_queryset(self):
        return Purchase.objects.exclude(status__in=["COMPLETED", "CANCELED"]).order_by('-created_at')


class MyPurchaseListView(UserPassesTestMixin, ListView):
    login_url = '/login/'
    permission_denied_message = 'Acesso apenas para clientes.'
    model = Purchase
    context_object_name = 'purchases'
    template_name = 'MyPurchaseList.html'

    def test_func(self):
        if (hasattr(self.request.user, 'serviceclient')):
            return True
        else:
            return False

    def get_queryset(self):
        return Purchase.objects.filter(client=self.request.user).order_by('-created_at')


class RegisterPurchaseView(UserPassesTestMixin, FormView):
    login_url = '/login/'
    permission_denied_message = "Acesso ao painel negado."
    form_class = RegisterPurchaseForm
    template_name = 'login-files/RegisterPurchase.html'

    def get_context_data(self, **kwargs):
        data = super(RegisterPurchaseView, self).get_context_data(**kwargs)
        if self.request.POST:
            data['baskets_formset'] = PurchaseBasketFormSet(self.request.POST, prefix="baskets")
        else:
            data['baskets_formset'] = PurchaseBasketFormSet(prefix="baskets")
        return data

    def test_func(self):
        if(hasattr(self.request.user, 'serviceadm')):
            return True
        else:
            return False

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        data = form.cleaned_data
        context = self.get_context_data()
        baskets = context['baskets_formset']

        user_data = {}
        client_data = {}
        user_data['first_name'] = data['first_name']
        user_data['last_name'] = data['last_name']
        user_data['username'] = data['cpf']
        client_data['phone'] = data['phone']
        client_data['cep'] = data['cep']
        client_data['state'] = data['state']
        client_data['city'] = data['city']
        client_data['district'] = data['district']
        client_data['address'] = data['address']
        client_data['number'] = data['number']
        client_data['complement'] = data['complement']
        client_data['cpf'] = data['cpf']
        if baskets.is_valid():
            try:
                client = ServiceClient.objects.get(cpf=data['cpf'])
                user_client = client.user
            except:
                user_client = User(**user_data)
                user_client.set_unusable_password()
                user_client.save()
                client = ServiceClient(user=user_client, **client_data)
                client.save()

            new_purchase = Purchase.objects.create(client=user_client)
            baskets.instance = new_purchase
            baskets.save()
            new_purchase = Purchase.objects.create(client=user_client, cart=new_cart)
            messages.success(self.request, 'Compra registrada com sucesso.')
        else:
            return self.form_invalid(form)
        return HttpResponseRedirect(reverse('purchase_cotrol', args=[new_purchase.pk]))

    def form_invalid(self, form):
        print(form.errors)
        messages.error(self.request, 'Não foi possível registrar a compra.')
        return super(RegisterPurchaseView, self).form_invalid(form)



def buying_view (request):
    if not request.user.is_authenticated:
        return redirect('%s?next=%s' % ('/login/', request.path))
    elif (not hasattr(request.user, 'serviceclient')):
        messages.error(request, "Acesso apenas para clientes.")
        raise PermissionDenied("Acesso apenas para clientes.")
    else:
        user_client = request.user
        client = user_client.serviceclient
        purchase = Purchase.objects.create(client=user_client)
        for cart_tuple in client.cart.cartproduct_set.all():
            produto = cart_tuple.product
            estoque = produto.stock
            if estoque >= cart_tuple.qnt:
                produto.stock = estoque - cart_tuple.qnt
                produto.sold = produto.sold + cart_tuple.qnt
                produto.save()
                PurchaseProduct.objects.create(purchase=purchase, product=produto, qnt=cart_tuple.qnt)
            else:
                messages.error(request, 'Não há ' + produto.name + ' suficiente.')
        for cart_tuple in client.cart.cartbasket_set.all():
            pacote = cart_tuple.basket
            estoque = pacote.stock
            if estoque >= cart_tuple.qnt:
                pacote.stock = estoque - cart_tuple.qnt
                pacote.sold = pacote.sold + cart_tuple.qnt
                pacote.save()
                PurchaseBasket.objects.create(purchase=purchase, basket=pacote, qnt=cart_tuple.qnt)
            else:
                messages.error(request, 'Não há ' + pacote.name + ' suficiente.')
        if client.cart.frete == 0:
           messages.error(request, 'Calcule o frete antes de comprar!')
           return HttpResponseRedirect(reverse('confirm_purchase'))
        else:
           client.cart.cartproduct_set.all().delete()
           client.cart.cartbasket_set.all().delete()
           client.cart.frete = 0
           messages.success(request, "Compra Realizada")
           return HttpResponseRedirect(reverse('purchase', args=[purchase.pk]))
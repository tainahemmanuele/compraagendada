from django.contrib.auth.mixins import UserPassesTestMixin
from django.contrib import messages
from django.views.generic import *
from django.core.exceptions import PermissionDenied
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.urls import reverse
from app.models import Cart, CartBasket, CartProduct, Product, Basket
from app.forms import CartBasketFormSet, CartProductFormSet, FreteForm
from app.views import Correios


class ShowCartView(UserPassesTestMixin, DetailView, FormView):
    login_url = "/login/"
    permission_denied_message = 'Acesso apenas para clientes.'
    template_name = 'ConfirmPurchase.html'
    model = Cart
    context_object_name = 'cart'
    form_class = FreteForm
    success_url = '/my_cart'

    def get_object(self):
        self.request.user.serviceclient.cart.frete = 0
        return self.request.user.serviceclient.cart

    def test_func(self):
        if (hasattr(self.request.user, 'serviceclient')):
            return True
        else:
            return False

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)


    def form_valid(self, form):
        data = form.cleaned_data
        client_data = {}
        client_data['cep'] = data['cep']
        cart = self.request.user.serviceclient.cart
        fields = {
            "cod": Correios.Correios.SEDEX,
            "GOCEP": "58400970",
            "HERECEP": client_data['cep'],
            "peso": "1",
            "formato": "1",  # caixa/pacote
            "comprimento": "18",
            "altura": "9",
            "largura": "13.5",
            "diametro": "0"
        }

        frete = float(Correios.Correios.frete(Correios.Correios,**fields)['Valor'].replace(',','.'))
        frete_print = Correios.Correios.frete(Correios.Correios,**fields)['Valor'].replace(',','.')

        if data['cep']:
            cart.frete = frete
            cart.save()
            messages.success(self.request, 'O valor do frete é de: ' +  frete_print)
        else:
            return self.form_invalid(form)

        return super(ShowCartView, self).form_invalid(form)

    def form_invalid(self, form):
        print(form.errors)
        messages.error(self.request, 'Não foi possível calcular o frete')
        return super(ShowCartView, self).form_invalid(form)


class EditCartView(UserPassesTestMixin, UpdateView):
    login_url = '/login/'
    permission_denied_message = "Acesso apenas para clientes."
    template_name = 'EditCart.html'
    model = Cart
    fields = []
    success_url = '/my_cart'

    def get_object(self):
        self.request.user.serviceclient.cart.frete = 0
        return self.request.user.serviceclient.cart

    def get_context_data(self, **kwargs):
        data = super(EditCartView, self).get_context_data(**kwargs)
        if self.request.POST:
            data['products_formset'] = CartProductFormSet(self.request.POST, prefix="products", instance=self.object)
            data['baskets_formset'] = CartBasketFormSet(self.request.POST, prefix="baskets", instance=self.object)
        else:
            data['products_formset'] = CartProductFormSet(prefix="products", instance=self.object)
            data['baskets_formset'] = CartBasketFormSet(prefix="baskets", instance=self.object)
        return data

    def test_func(self):
        if (hasattr(self.request.user, 'serviceclient')):
            return True
        else:
            return False

    def form_valid(self, form):
        context = self.get_context_data()
        products = context['products_formset']
        baskets = context['baskets_formset']

        if products.is_valid() and baskets.is_valid():
            products.instance = self.object
            products.save()
            baskets.instance = self.object
            baskets.save()
            messages.success(self.request, 'Carrinho Atualizado.')
            return super(EditCartView, self).form_valid(form)
        else:
            return super(EditCartView, self).form_invalid(form)

    def form_invalid(self, form):
        print(form.errors)
        messages.error(self.request, 'Não foi possível atualizar o carrinho.')
        return super(EditCartView, self).form_invalid(form)


def add_Product(request, pk):
    if not request.user.is_authenticated:
        return redirect('%s?next=%s' % ('/login/', request.path))
    elif (not hasattr(request.user, 'serviceclient')):
        messages.error(request, "Acesso apenas para clientes.")
        raise PermissionDenied("Acesso apenas para clientes.")
    else:
        pessoa = request.user.serviceclient
        produto = Product.objects.get(id=pk)
        estoque = produto.stock
        if estoque > 0:
            if pessoa.cart.cartproduct_set.filter(product=produto):
                cont = pessoa.cart.cartproduct_set.get(product=produto)
                qnt = cont.qnt
                if estoque - qnt > 0:
                    cont.qnt = qnt + 1
                    cont.save()
                else:
                    messages.error(request, "Produto não tem estoque suficiente")
                    return HttpResponseRedirect('/product/' + pk)
            else:
                CartProduct.objects.create(cart=pessoa.cart, product=produto)
        else:
            messages.error(request, "Produto fora de estoque")
            return HttpResponseRedirect('/product/' + pk)
        messages.success(request, "Produto adicionado ao carrinho")
        return HttpResponseRedirect('/my_cart')


def add_Basket(request, pk):
    if not request.user.is_authenticated:
        return redirect('%s?next=%s' % ('/login/', request.path))
    elif (not hasattr(request.user, 'serviceclient')):
        messages.error(request, "Acesso apenas para clientes.")
        raise PermissionDenied("Acesso apenas para clientes.")
    else:
        pessoa = request.user.serviceclient
        cesta = Basket.objects.get(id=pk)
        estoque = cesta.stock
        if estoque > 0:
            if pessoa.cart.cartbasket_set.filter(basket=cesta):
                cont = pessoa.cart.cartbasket_set.get(basket=cesta)
                if estoque - cont.qnt > 0:
                    cont.qnt = cont.qnt + 1
                    cont.save()
                else:
                    messages.error(request, "Pacote não tem estoque suficiente")
                    return HttpResponseRedirect('/basket/' + pk)
            else:
                CartBasket.objects.create(cart=pessoa.cart, basket=cesta)
        else:
            messages.error(request, "Pacote fora de estoque")
            return HttpResponseRedirect('/basket/' + pk)
        messages.success(request, "Cesta adicionado ao carrinho")
        return HttpResponseRedirect('/my_cart')

from django.contrib import messages
from django.contrib.auth.models import User
from django.views.generic import *
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.core.exceptions import PermissionDenied

from app.forms import ServiceClientForm, EditClientForm
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.mixins import UserPassesTestMixin
from app.models import ServiceClient
from django.contrib.auth.views import PasswordChangeView


class RegisterClientView(FormView):
    """
    Displays the login form.
    """
    template_name = 'registers/ServiceClientRegister.html'
    form_class = ServiceClientForm
    success_url = '/login'

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        data = form.cleaned_data
        aux_obj = User.objects.filter(username=data['username'])
        if len(aux_obj) > 0:
            return self.form_invalid(form)
        user_data = {}
        client_data = {}
        user_data['first_name'] = data['first_name']
        user_data['last_name'] = data['last_name']
        user_data['email'] = data['email']
        user_data['username'] = data['username']
        user_data['password'] = data['password']
        client_data['phone'] = data['phone']
        client_data['cep'] = data['cep']
        client_data['state'] = data['state']
        client_data['city'] = data['city']
        client_data['district'] = data['district']
        client_data['address'] = data['address']
        client_data['number'] = data['number']
        client_data['complement'] = data['complement']
        client_data['cpf'] = data['cpf']
        if data['username'] and data['password']:
            new_user = User.objects.create_user(**user_data)
            new_client = ServiceClient(user=new_user, **client_data)
            new_client.save()
            messages.success(self.request, 'Novo usuário cadastrado com sucesso.')
        else:
            return self.form_invalid(form)
        return super(RegisterClientView, self).form_valid(form)

    def form_invalid(self, form):
        print(form.errors)
        messages.error(self.request, 'Não foi possível cadastrar.')
        return super(RegisterClientView, self).form_invalid(form)


class EditClientView(UserPassesTestMixin, UpdateView):
    login_url = '/login/'
    permission_denied_message = 'Acesso apenas para clientes.'
    model = User
    form_class = EditClientForm
    template_name = 'registers/ServiceClientEdit.html'

    def test_func(self):
        if(hasattr(self.request.user, 'serviceclient')):
            return True
        else:
            return False

    def get_object(self):
        return self.request.user

    def get_initial(self):
        initial = super(EditClientView, self).get_initial()
        try:
            client = self.object.serviceclient
        except:
            pass
        else:
            initial['phone'] = client.phone
            initial['cep'] = client.cep
            initial['state'] = client.state
            initial['city'] = client.city
            initial['district'] = client.district
            initial['address'] = client.address
            initial['number'] = client.number
            initial['complement'] = client.complement
        return initial

    def form_valid(self, form):
        data = form.cleaned_data
        client = self.object.serviceclient
        client.phone = data['phone']
        client.cep = data['cep']
        client.address = data['address']
        client.number = data['number']
        client.state = data['state']
        client.city = data['city']
        client.district = data['district']
        client.complement = data['complement']
        client.save()

        return super(EditClientView, self).form_valid(form)

    def form_invalid(self, form):
        print(form.errors)
        return super(EditClientView, self).form_invalid(form)

    def get_success_url(self):
        return reverse('detail')


class DetailClientView(UserPassesTestMixin, DetailView):
    login_url = "/login/"
    permission_denied_message = 'Acesso apenas para clientes.'
    template_name = 'DetailClient.html'
    model = User
    context_object_name = 'user_l'

    def get_object(self):
        return self.request.user

    def test_func(self):
        if(hasattr(self.request.user, 'serviceclient')):
            return True
        else:
            return False

def delete_client(request):
    if not request.user.is_authenticated:
        messages.error(request, "Usuário precisa estar logado para esta operação")
        raise PermissionDenied("Usuário precisa estar logado para esta operação")
    elif(not hasattr(request.user, 'serviceclient')):
        messages.error(request, "Acesso apenas para clientes.")
        raise PermissionDenied("Acesso apenas para clientes.")
    else:
        pessoa = request.user.serviceclient
        pessoa.delete()
        messages.success(request, "Conta apagada com sucesso")
        return HttpResponseRedirect('/')


class ChangePasswordClientView(PasswordChangeView):
    template_name = 'registers/ServiceClientChangePassword.html'
    permission_denied_message = 'Acesso apenas para clientes.'
    success_url = '/'
    form_class = PasswordChangeForm

    def test_func(self):
        if(hasattr(self.request.user, 'serviceclient')):
            return True
        else:
            return False

    def get_success_url(self):
        messages.success(self.request, "Senha alterada com sucesso")
        return reverse('detail')
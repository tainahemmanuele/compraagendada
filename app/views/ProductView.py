from base64 import b64encode
import pyimgur as pyimgur
from django.contrib.auth.mixins import UserPassesTestMixin
from django.contrib import messages
from django.shortcuts import render
from django.views.generic import *
from django.core.exceptions import PermissionDenied
from app.forms import ProductForm, EditProductForm, ProductTypeForm
from django.http import HttpResponseRedirect
from app.models import Product, ProductType
from django.urls import reverse


class RegisterProductView(UserPassesTestMixin, FormView):
    login_url = '/login/'
    permission_denied_message = 'Acesso ao painel negado.'
    template_name = 'registers/ProductRegister.html'
    form_class = ProductForm
    success_url = '/painel/products'

    def test_func(self):
        if(hasattr(self.request.user, 'serviceadm')):
            return True
        else:
            return False

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        data = form.cleaned_data
        product_data = {}
        product_data['name'] = data['name']
        product_data['brand'] = data['brand']
        product_data['distributor'] = data['distributor']
        product_data['cost'] = float(data['cost'])
        product_data['profit'] = float(data['profit'])
        product_data['type'] = ProductType.objects.get(id=data['type'])
        product_data['stock'] = int(data['stock'])
        if product_data['name'] and product_data['cost']:
            try:
                CLIENT_ID = "9371764cfa1bb97"
                data = b64encode(self.request.FILES['file'].read())
                client = pyimgur.Imgur(CLIENT_ID)
                r = client._send_request('https://api.imgur.com/3/image', method='POST', params={'image': data})
                file = r['link']
            except (Exception,):
                file = 'http://placehold.it/160x160'
            product_data['photo'] = file
            new_product = Product(**product_data)
            new_product.save()
            messages.success(self.request, 'Produto cadastrado com sucesso.')
        else:
            return self.form_invalid(form)
        return super(RegisterProductView, self).form_valid(form)

    def form_invalid(self, form):
        print(form.errors)
        messages.error(self.request, 'Não foi possível cadastrar o Produto.')
        return super(RegisterProductView, self).form_invalid(form)


class ProductView():
    login_url = '/login/'
    permission_denied_message = 'Acesso ao painel negado.'
    model = Product
    form_class = ProductForm
    template_name = 'Product.html'
    slug_field = 'pk'
    slug_url_kwarg = 'pk'


    def product_detail(request, product_id):
        if not request.user.is_authenticated:
            messages.error(request, "Usuário precisa estar logado para esta operação")
            raise PermissionDenied("Usuário precisa estar logado para esta operação")
        elif(not hasattr(request.user, 'serviceadm')):
            messages.error(request, "Acesso ao painel negado.")
            raise PermissionDenied("Acesso ao painel negado.")
        else:
          product = Product.objects.all().get(id=product_id)
          return render(request, 'Product.html',
                  {'product': product})


class ProductClientView():
    model = Product
    form_class = ProductForm
    template_name = 'ProductClient.html'
    slug_field = 'pk'
    slug_url_kwarg = 'pk'

    def product_detail_client(request, product_id):
       product = Product.objects.all().get(id=product_id)
       return render(request, 'ProductClient.html',
                  {'product': product})


class ProductListView(UserPassesTestMixin, ListView):
    login_url = '/login/'
    permission_denied_message = "Acesso ao painel negado."
    model = Product
    context_object_name = 'products'
    template_name = 'ProductList.html'

    def test_func(self):
        if(hasattr(self.request.user, 'serviceadm')):
            return True
        else:
            return False

    def get_queryset(self):
        return Product.objects.order_by('type', 'cost')


class RemovedProductListView(UserPassesTestMixin, ListView):
    login_url = '/login/'
    permission_denied_message = "Acesso ao painel negado."
    model = Product
    context_object_name = 'products'
    template_name = 'login-files/DeletedProductList.html'

    def test_func(self):
        if(hasattr(self.request.user, 'serviceadm')):
            return True
        else:
            return False

    def get_queryset(self):
        return Product.all_objects.filter(available=False).order_by('type', 'cost')


def delete_product (request, pk):
    if not request.user.is_authenticated:
        messages.error(request, "Usuário precisa estar logado para esta operação")
        raise PermissionDenied("Usuário precisa estar logado para esta operação")
    elif(not hasattr(request.user, 'serviceadm')):
        messages.error(request, "Acesso ao painel negado.")
        raise PermissionDenied("Acesso ao painel negado.")
    else:
        produto = Product.objects.get(id=pk)
        produto.delete()
        messages.success(request, "Produto apagado com sucesso")
        return HttpResponseRedirect('/painel/products')


class EditProductView(UserPassesTestMixin, UpdateView):
    login_url = '/login/'
    model = Product
    permission_denied_message = 'Acesso negado.'
    form_class = EditProductForm
    template_name = 'registers/ProductEdit.html'
    success_url = '/painel/products'


    def test_func(self):
        if(hasattr(self.request.user, 'serviceadm')):
            return True
        else:
            return False

    def get_initial(self):
        initial = super(EditProductView, self).get_initial()
        try:
            product = self.object.product
        except:
            pass
        else:
            initial['name'] = product.name
            initial['brand'] = product.brand
            initial['distributor'] = product.distributor
            initial['cost'] = product.cost
            initial['profit'] = product.profit
            initial['type'] = product.type.id
            initial['stock'] = product.stock
        return initial

    def form_valid(self, form):
        try:
            CLIENT_ID = "9371764cfa1bb97"
            data1 = b64encode(self.request.FILES['file'].read())
            client = pyimgur.Imgur(CLIENT_ID)
            r = client._send_request('https://api.imgur.com/3/image', method='POST', params={'image': data1})
            form.instance.photo = r['link']
        except (Exception,):
            pass

        if "cost" in form.changed_data:
            dif = form.cleaned_data['cost'] - form.initial['cost']
            for cont in self.object.basketcontent_set.all():
                total_dif = dif * cont.qnt
                basket = cont.basket
                basket.cost = basket.cost + total_dif
                basket.save()

        self.object = form.save()
        messages.success(self.request, 'Produto editado com sucesso.')
        return super(EditProductView, self).form_valid(form)

    def form_invalid(self, form):
        print(form.errors)
        messages.error(self.request, 'Não foi possível editar o produto.')
        return super(EditProductView, self).form_invalid(form)

    def get_success_url(self):
        return reverse('product', args=[self.object.pk])


class NewProductTypeView(UserPassesTestMixin, FormView):
    form_class = ProductTypeForm
    login_url = '/login/'
    permission_denied_message = 'Acesso ao painel negado.'
    template_name = 'login-files/NewProductType.html'
    success_url = '/painel/new_product_type'

    def test_func(self):
        if(hasattr(self.request.user, 'serviceadm')):
            return True
        else:
            return False

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        data = form.cleaned_data
        if data['name']:
            new_product_type = ProductType(name=data['name'])
            new_product_type.save()
            messages.success(self.request, 'Tipo de Produto cadastrado com sucesso.')
        else:
         return self.form_invalid(self, form)
        return super(NewProductTypeView, self).form_valid(form)

    def form_invalid(self, form):
        print(form.errors)
        messages.error(self.request, 'Não foi possível cadastrar o Tipo de Produto.')
        return super(NewProductTypeView, self).form_invalid(form)


class ProductTypeListView(UserPassesTestMixin, ListView):
    login_url = '/login/'
    permission_denied_message = "Acesso ao painel negado."
    model = Product
    context_object_name = 'types'
    template_name = 'login-files/ProductTypeList.html'

    def test_func(self):
        if(hasattr(self.request.user, 'serviceadm')):
            return True
        else:
            return False

    def get_queryset(self):
        return ProductType.objects.all()


def delete_product_type (request, pk):
    if not request.user.is_authenticated:
        messages.error(request, "Usuário precisa estar logado para esta operação")
        raise PermissionDenied("Usuário precisa estar logado para esta operação")
    elif(not hasattr(request.user, 'serviceadm')):
        messages.error(request, "Acesso ao painel negado.")
        raise PermissionDenied("Acesso ao painel negado.")
    else:
        produt_type = ProductType.objects.get(id=pk)
        produt_type.delete()
        messages.success(request, "Produto apagado com sucesso")
        return HttpResponseRedirect('/painel/product_type_list')
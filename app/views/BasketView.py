from base64 import b64encode
import pyimgur as pyimgur
from django.contrib import messages
from django.views.generic import *
from django.shortcuts import render
from app.forms import BasketSkelForm, AutoBasketForm, BasketForm, BasketContentFormSet
from django.contrib.auth.mixins import UserPassesTestMixin
from app.models import BasketSkel, Basket, Product, ContentQnt, ProductType, BasketContent
from django.http import HttpResponseRedirect
from django.core.exceptions import PermissionDenied


class CreateBasketSkelView(UserPassesTestMixin, FormView):
    login_url = '/login/'
    permission_denied_message = "Acesso ao painel negado."
    template_name = 'login-files/CreateBasketSkel.html'
    form_class = BasketSkelForm
    success_url = '/painel'

    def test_func(self):
        if(hasattr(self.request.user, 'serviceadm')):
            return True
        else:
            return False

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        data = form.cleaned_data
        basketskel_name = data['name']
        cont = dict(data)
        cont.pop('name')
        cont = {k: v for k, v in cont.items() if v > '0'}

        if data['name']:
            new_basketskel = BasketSkel.objects.create(name=basketskel_name)
            new_basketskel.save()
            for t, q in cont.items():
                prodtype = ProductType.objects.get(name=t)
                ContentQnt.objects.create(basketskel=new_basketskel, type=prodtype, qnt=q).save()
            messages.success(self.request, 'Nova cesta criada com sucesso.')
        else:
            return self.form_invalid(form)
        return super(CreateBasketSkelView, self).form_valid(form)

    def form_invalid(self, form):
        print(form.errors)
        messages.error(self.request, 'Não foi possível criar a cesta.')
        return super(CreateBasketSkelView, self).form_invalid(form)


class CreateBasketView(UserPassesTestMixin, CreateView):
    login_url = '/login/'
    permission_denied_message = "Acesso ao painel negado."
    template_name = 'login-files/CreateBasket.html'
    form_class = BasketForm
    success_url = 'painel/basket_list'

    def get_context_data(self, **kwargs):
        data = super(CreateBasketView, self).get_context_data(**kwargs)
        if self.request.POST:
            data['products_formset'] = BasketContentFormSet(self.request.POST, prefix="products")
        else:
            data['products_formset'] = BasketContentFormSet(prefix="products")
        return data

    def test_func(self):
        if(hasattr(self.request.user, 'serviceadm')):
            return True
        else:
            return False

    def form_valid(self, form):
        context = self.get_context_data()
        products = context['products_formset']
        try:
            CLIENT_ID = "9371764cfa1bb97"
            data1 = b64encode(self.request.FILES['file'].read())
            client = pyimgur.Imgur(CLIENT_ID)
            r = client._send_request('https://api.imgur.com/3/image', method='POST', params={'image': data1})
            file = r['link']
        except (Exception,):
            file = 'http://placehold.it/160x160'

        form.instance.photo = file

        if products.is_valid():
            cost = 0.0
            print(products)
            for form_prod in [f for f in products if f.cleaned_data and not f.cleaned_data.get('DELETE', False)]:
                data = form_prod.cleaned_data
                qnt = data.get('qnt', 0)
                prod = data['product']
                cost += int(qnt) * float(prod.cost)
            form.instance.cost = cost
            self.object = form.save()
            products.instance = self.object
            products.save()
            messages.success(self.request, 'Nova cesta criada com sucesso.')
            return super(CreateBasketView, self).form_valid(form)
        else:
            return self.form_invalid(form)

    def form_invalid(self, form):
        print(form.errors)
        messages.error(self.request, 'Não foi possível criar a cesta.')
        return super(CreateBasketView, self).form_invalid(form)


class EditBasketView(UserPassesTestMixin, UpdateView):
    login_url = '/login/'
    model = Basket
    permission_denied_message = "Acesso ao painel negado."
    template_name = 'login-files/CreateBasket.html'
    form_class = BasketForm
    success_url = 'painel/basket_list'

    def get_context_data(self, **kwargs):
        data = super(EditBasketView, self).get_context_data(**kwargs)
        if self.request.POST:
            data['products_formset'] = BasketContentFormSet(self.request.POST, prefix="products", instance=self.object)
        else:
            data['products_formset'] = BasketContentFormSet(prefix="products", instance=self.object)
        return data

    def test_func(self):
        if(hasattr(self.request.user, 'serviceadm')):
            return True
        else:
            return False

    def form_valid(self, form):
        context = self.get_context_data()
        products = context['products_formset']
        try:
            CLIENT_ID = "9371764cfa1bb97"
            data1 = b64encode(self.request.FILES['file'].read())
            client = pyimgur.Imgur(CLIENT_ID)
            r = client._send_request('https://api.imgur.com/3/image', method='POST', params={'image': data1})
            form.instance.photo = r['link']
        except (Exception,):
            pass

        if products.is_valid():
            cost = 0.0
            print(products)
            for form_prod in [f for f in products if f.cleaned_data and not f.cleaned_data.get('DELETE', False)]:
                data = form_prod.cleaned_data
                qnt = data.get('qnt', 0)
                prod = data['product']
                cost += int(qnt) * float(prod.cost)
            form.instance.cost = cost
            self.object = form.save()
            products.instance = self.object
            products.save()
            messages.success(self.request, 'Cesta alterada com sucesso.')
            return super(EditBasketView, self).form_valid(form)
        else:
            print(products.errors)
            return self.form_invalid(form)

    def form_invalid(self, form):
        print(form.errors)
        messages.error(self.request, 'Não foi possível editar a cesta.')
        return super(EditBasketView, self).form_invalid(form)


class AutoCreateBasketView(UserPassesTestMixin, FormView):
    login_url = '/login/'
    permission_denied_message = "Acesso ao painel negado."
    template_name = 'login-files/AutoCreateBasket.html'
    form_class = AutoBasketForm
    success_url = 'painel/basket_list'

    def test_func(self):
        if(hasattr(self.request.user, 'serviceadm')):
            return True
        else:
            return False

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        data = form.cleaned_data
        basket_profit = float(data['profit'])

        if data['basketskel']:
            try:
                CLIENT_ID = "9371764cfa1bb97"
                data1 = b64encode(self.request.FILES['file'].read())
                client = pyimgur.Imgur(CLIENT_ID)
                r = client._send_request('https://api.imgur.com/3/image', method='POST', params={'image': data1})
                file = r['link']
            except (Exception,):
                file = 'http://placehold.it/160x160'

            basketskel = BasketSkel.objects.get(id=data['basketskel'])
            new_basket = Basket.objects.create(name=basketskel.name, profit=basket_profit, photo=file)
            cost = 0.0

            for cont in basketskel.contentqnt_set.all():
                prod_type = cont.type
                qnt = cont.qnt
                prod = Product.objects.filter(type=prod_type).order_by('cost').first()
                if (prod):
                    BasketContent.objects.create(basket=new_basket, product=prod, qnt=qnt)
                    cost += int(qnt) * float(prod.price)

            new_basket.cost=cost
            new_basket.save()

            messages.success(self.request, 'Nova cesta criada com sucesso.')
        else:
            return self.form_invalid(form)
        return super(AutoCreateBasketView, self).form_valid(form)

    def form_invalid(self, form):
        print(form.errors)
        messages.error(self.request, 'Não foi possível criar a cesta.')
        return super(AutoCreateBasketView, self).form_invalid(form)


class BasketView(ListView):
    model = BasketSkel
    form_class = AutoBasketForm
    template_name = 'Basket.html'
    slug_field = 'pk'
    slug_url_kwarg = 'pk'

    def basket_detail(request, basket_id):
        basket = Basket.objects.get(id=basket_id)
        return render(request, 'Basket.html',
                      {'basket': basket})


class BasketListView(UserPassesTestMixin, ListView):
    login_url = '/login/'
    permission_denied_message = 'Acesso ao painel negado.'
    model = Basket
    context_object_name = 'baskets'
    template_name = 'login-files/BasketList.html'

    def test_func(self):
        if(hasattr(self.request.user, 'serviceadm')):
            return True
        else:
            return False

    def get_queryset(self):
        return Basket.objects.order_by('-created_at')


class BasketSkelListView(UserPassesTestMixin, ListView):
    login_url = '/login/'
    permission_denied_message = 'Acesso ao painel negado.'
    model = BasketSkel
    context_object_name = 'basketskels'
    template_name = 'login-files/BasketSkelList.html'

    def test_func(self):
        if(hasattr(self.request.user, 'serviceadm')):
            return True
        else:
            return False

    def get_queryset(self):
        return BasketSkel.objects.order_by('-name')


class BasketDetailView(UserPassesTestMixin, DetailView):
    login_url = '/login/'
    permission_denied_message = 'Acesso ao painel negado.'
    model = BasketSkel
    form_class = AutoBasketForm
    template_name = 'login-files/BasketDetail.html'
    slug_field = 'pk'
    slug_url_kwarg = 'pk'

    def test_func(self):
        if(hasattr(self.request.user, 'serviceadm')):
            return True
        else:
            return False

    def basket_detail(request, basket_id):
        basket = Basket.objects.get(id=basket_id)
        return render(request, 'login-files/BasketDetail.html',
                      {'basket': basket})

def delete_basket (request, pk):
    if(not request.user.is_authenticated):
        messages.error(request, "Usuário precisa estar logado para esta operação")
        raise PermissionDenied("Usuário precisa estar logado para esta operação")
    elif(not hasattr(request.user, 'serviceadm')):
        messages.error(request, "Acesso ao painel negado.")
        raise PermissionDenied("Acesso ao painel negado.")
    else:
        cesta = Basket.objects.get(id=pk)
        cesta.delete()
        messages.success(request, "Cesta apagada com sucesso")
        return HttpResponseRedirect('/painel/basket_list')

def delete_basketskel (request, pk):
    if(not request.user.is_authenticated):
        messages.error(request, "Usuário precisa estar logado para esta operação")
        raise PermissionDenied("Usuário precisa estar logado para esta operação")
    elif(not hasattr(request.user, 'serviceadm')):
        messages.error(request, "Acesso ao painel negado.")
        raise PermissionDenied("Acesso ao painel negado.")
    else:
        estrutura_cesta = BasketSkel.objects.get(id=pk)
        estrutura_cesta.delete()
        messages.success(request, "Estrutura de cesta apagada com sucesso")
        return HttpResponseRedirect('/painel/basketskel_list')
from django.contrib import messages
from django.contrib.auth.models import User
from django.shortcuts import render
from django.views.generic import *
import pyimgur as pyimgur
from django.contrib.auth.mixins import LoginRequiredMixin
from app.forms import ServiceClientForm, ServiceAdmForm, ProductForm
from app.models import  Product, Basket, BasketSkel



class ShowcaseViewProduct(ListView):
    model = Product
    context_object_name = 'products'
    template_name = 'ShowcaseProducts.html'

    def get_queryset(self):
        return Product.objects.order_by('-created_at')


class ShowcaseViewBasket(ListView):
    model = Basket
    context_object_name = 'baskets'
    template_name = 'ShowcaseBaskets.html'

    def get_queryset(self):
        return Basket.objects.order_by('-created_at')
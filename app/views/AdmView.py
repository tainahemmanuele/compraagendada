from django.contrib import messages
from django.contrib.auth.models import User
from django.views.generic import *
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.core.exceptions import PermissionDenied

from app.forms import ServiceAdmForm, EditAdmForm
from app.models import ServiceAdm
from django.contrib.auth.mixins import UserPassesTestMixin
from django.contrib.auth.views import PasswordChangeView
from django.contrib.auth.forms import PasswordChangeForm

class RegisterAdmView(FormView):
    """
    Displays the login form.
    """
    template_name = 'registers/ServiceAdmRegister.html'
    form_class = ServiceAdmForm
    success_url = '/'

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        data = form.cleaned_data
        aux_obj = User.objects.filter(username=data['username'])
        if len(aux_obj) > 0:
            return self.form_invalid(form)
        user_data = {}
        adm_data = {}
        user_data['first_name'] = data['first_name']
        user_data['last_name'] = data['last_name']
        user_data['email'] = data['email']
        user_data['username'] = data['username']
        user_data['password'] = data['password']
        adm_data['phone'] = data['phone']
        if data['username'] and data['password']:
            new_user = User.objects.create_user(**user_data)
            new_adm = ServiceAdm(user=new_user, **adm_data)
            new_adm.save()
            messages.success(self.request, 'Novo administrador cadastrado com sucesso.')
        else:
            return self.form_invalid(form)
        return super(RegisterAdmView, self).form_valid(form)

    def form_invalid(self, form):
        print(form.errors)
        messages.error(self.request, 'Não foi possível cadastrar.')
        return super(RegisterAdmView, self).form_invalid(form)


class EditAdmView(UserPassesTestMixin, UpdateView):
    login_url = '/login/'
    permission_denied_message = "Acesso ao painel negado."
    model = User
    form_class = EditAdmForm
    template_name = 'login-files/ServiceAdmEdit.html'

    def test_func(self):
        if(hasattr(self.request.user, 'serviceadm')):
            return True
        else:
            return False

    def get_object(self):
        return self.request.user

    def get_initial(self):
        initial = super(EditAdmView, self).get_initial()
        try:
            adm = self.object.serviceadm
        except:
            pass
        else:
            initial['phone'] = adm.phone
        return initial

    def form_valid(self, form):
        data = form.cleaned_data
        adm = self.object.serviceadm
        adm.phone = data['phone']
        adm.save()

        return super(EditAdmView, self).form_valid(form)

    def form_invalid(self, form):
        print(form.errors)
        return super(EditAdmView, self).form_invalid(form)

    def get_success_url(self):
        return reverse('detail_adm')


class DetailAdmView(UserPassesTestMixin, DetailView):
    login_url = "/login/"
    permission_denied_message = 'Acesso ao painel negado.'
    template_name = 'login-files/DetailAdm.html'
    model = User
    context_object_name = 'user_l'

    def test_func(self):
        if(hasattr(self.request.user, 'serviceadm')):
            return True
        else:
            return False

    def get_object(self):
        return self.request.user

def delete_adm(request):
    if not request.user.is_authenticated:
        messages.error(request, "Usuário precisa estar logado para esta operação")
        raise PermissionDenied("Usuário precisa estar logado para esta operação")
    elif(not hasattr(request.user, 'serviceadm')):
        messages.error(request, "Acesso apenas para clientes.")
        raise PermissionDenied("Acesso apenas para clientes.")
    else:
        pessoa = request.user
        pessoa.delete()
        messages.success(request, "Conta apagada com sucesso")
        return HttpResponseRedirect('/')


class ChangePasswordAdmView(PasswordChangeView):
    template_name = 'login-files/ServiceAdmChangePassword.html'
    permission_denied_message = 'Acesso apenas para administradores.'
    success_url = '/'
    form_class = PasswordChangeForm

    def test_func(self):
        if(hasattr(self.request.user, 'serviceadm')):
            return True
        else:
            return False
    def get_success_url(self):
        messages.success(self.request, "Senha alterada com sucesso")
        return reverse('detail_adm')
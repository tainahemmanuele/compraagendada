import re

from django.core.exceptions import ValidationError
from django.utils.translation import ugettext as _


class NumberValidator(object):
    def __init__(self, min_digits=0):
        self.min_digits = min_digits

    def validate(self, password, user=None):
        if not len(re.findall('\d', password)) >= self.min_digits:
            raise ValidationError(
                _("A senha deve conter pelo menos %d digito(s) (0-9)." % self.min_digits),
                code='password_no_number',
            )

    def get_help_text(self):
        return _(
            "Sua senha deve conter pelo menos %d digito(s) (0-9)." % self.min_digits
        )


class UppercaseValidator(object):
    def validate(self, password, user=None):
        if not re.findall('[A-Z]', password):
            raise ValidationError(
                _("A senha deve conter pelo menos uma letra maiúscula (A-Z)."),
                code='password_no_upper',
            )

    def get_help_text(self):
        return _(
            "Sua senha deve conter pelo menos uma letra maiúscula (A-Z)."
        )


class LowercaseValidator(object):
    def validate(self, password, user=None):
        if not re.findall('[a-z]', password):
            raise ValidationError(
                _("A senha deve conter pelo menos uma letra minúscula (a-z)."),
                code='password_no_lower',
            )

    def get_help_text(self):
        return _(
            "Sua senha deve conter pelo menos uma letra minúscula (a-z)."
        )
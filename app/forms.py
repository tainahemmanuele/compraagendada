from django import forms
from django.contrib.auth.models import User
from django.contrib.auth import password_validation
from app.models import *
from django.utils.translation import ugettext_lazy as _

class BaseForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(BaseForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'


class BaseAddressForm(BaseForm):
    cep = forms.CharField(max_length=10, required=False, widget=forms.TextInput(attrs={
    }))
    address = forms.CharField(max_length=50, required=False, widget=forms.TextInput(attrs={
    }))
    number = forms.CharField(max_length=5, required=False, widget=forms.TextInput(attrs={
    }))
    state = forms.CharField(max_length=30, required=False, widget=forms.TextInput(attrs={
    }))
    city = forms.CharField(max_length=30, required=False, widget=forms.TextInput(attrs={
    }))
    district = forms.CharField(max_length=45, required=False, widget=forms.TextInput(attrs={
    }))
    complement = forms.CharField(max_length=30, required=False, widget=forms.TextInput(attrs={
    }))


class ServiceClientForm(BaseAddressForm):
    error_messages = {'password_mismatch': _("As senhas digitadas são diferentes, por favor tente novamente.")}

    first_name = forms.CharField(widget=forms.TextInput(attrs={'required': True, 'maxlength': 200,
                                                               'placeholder': 'Nome',
                                                               }))
    last_name = forms.CharField(widget=forms.TextInput(attrs={'required': True, 'maxlength': 200,
                                                              'placeholder': 'Sobrenome',
                                                              }))
    email = forms.EmailField(widget=forms.EmailInput(attrs={'required': False, 'maxlength': 150,
                                                            'placeholder': 'Email',
                                                            }))
    phone = forms.CharField(widget=forms.TextInput(attrs={'required': False, 'maxlength': 16,
                                                             'placeholder': 'Telefone',
                                                             }))
    cpf = forms.CharField(widget=forms.TextInput(attrs={'required': True, 'maxlength': 150,
                                                        'placeholder': _('CPF'),
                                                        }))
    username = forms.CharField(widget=forms.TextInput(attrs={'required': True, 'maxlength': 200,
                                                             'placeholder': 'Nome de Usuario',
                                                             }))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'required': True,
                                                                 'placeholder': 'Senha',
                                                                 }),
                                help_text=password_validation.password_validators_help_text_html())
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={'required': True,
                                                                 'placeholder': 'Confirmar Senha',
                                                                 }),
                                help_text="Repita a senha digitada anteriormente.")

    def __init__(self, *args, **kwargs):
        super(ServiceClientForm, self).__init__(*args, **kwargs)

    def clean_first_name(self):
        name = self.cleaned_data.get("first_name")
        return name.title()

    def clean_last_name(self):
        name = self.cleaned_data.get("last_name")
        return name.title()

    def clean_password2(self):
        password1 = self.cleaned_data.get("password")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return password2

    def _post_clean(self):
        super()._post_clean()
        password = self.cleaned_data.get('password2')
        if password:
            try:
                password_validation.validate_password(password)
            except forms.ValidationError as error:
                self.add_error('password2', error)


class ServiceAdmForm(BaseForm):
    error_messages = {'password_mismatch': _("As senhas digitadas são diferentes, por favor tente novamente.")}

    first_name = forms.CharField(widget=forms.TextInput(attrs={'required': True, 'maxlength': 200,
                                                               'placeholder': 'Nome',
                                                               }))
    last_name = forms.CharField(widget=forms.TextInput(attrs={'required': True, 'maxlength': 200,
                                                              'placeholder': 'Sobrenome',
                                                              }))
    email = forms.EmailField(widget=forms.EmailInput(attrs={'required': False, 'maxlength': 150,
                                                            'placeholder': 'Email',
                                                            }))
    phone = forms.CharField(widget=forms.TextInput(attrs={'required': False, 'maxlength': 16,
                                                             'placeholder': 'Telefone',
                                                             }))
    username = forms.CharField(widget=forms.TextInput(attrs={'required': True, 'maxlength': 200,
                                                             'placeholder': 'Nome de Usuario',
                                                             }))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'required': True,
                                                                 'placeholder': 'Senha',
                                                                 }),
                                help_text=password_validation.password_validators_help_text_html())
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={'required': True,
                                                                 'placeholder': 'Confirmar Senha',
                                                                 }),
                                help_text="Repita a senha digitada anteriormente.")

    def __init__(self, *args, **kwargs):
        super(ServiceAdmForm, self).__init__(*args, **kwargs)

    def clean_first_name(self):
        name = self.cleaned_data.get("first_name")
        return name.title()

    def clean_last_name(self):
        name = self.cleaned_data.get("last_name")
        return name.title()

    def clean_password2(self):
        password1 = self.cleaned_data.get("password")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return password2

    def _post_clean(self):
        super()._post_clean()
        password = self.cleaned_data.get('password2')
        if password:
            try:
                password_validation.validate_password(password)
            except forms.ValidationError as error:
                self.add_error('password2', error)


class LoginForm(BaseForm):
    msgReq = 'data-parsley-required-message'
    username = forms.CharField(widget=forms.TextInput(attrs={'required': True,
                                                             'maxlength': 200,
                                                             'placeholder': 'Nome de Usuario',
                                                             msgReq: "Nome de usuario nao preenchido"}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'required': True,
                                                                 'placeholder': 'Senha',

                                                                 msgReq: "Senha nao preenchido"}))

class EditClientForm(forms.ModelForm, BaseAddressForm):
    first_name = forms.CharField(widget=forms.TextInput(attrs={'required': True, 'maxlength': 200,
                                                               'placeholder': 'Nome',
                                                               }))
    last_name = forms.CharField(widget=forms.TextInput(attrs={'required': True, 'maxlength': 200,
                                                              'placeholder': 'Sobrenome',
                                                              }))
    email = forms.EmailField(widget=forms.EmailInput(attrs={'required': False, 'maxlength': 150,
                                                            'placeholder': 'Email',
                                                            }))
    phone = forms.CharField(widget=forms.TextInput(attrs={'required': False, 'maxlength': 16,
                                                             'placeholder': 'Telefone',
        }))

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email']

    def __init__(self, *args, **kwargs):
        super(EditClientForm, self).__init__(*args, **kwargs)

    def clean_first_name(self):
        name = self.cleaned_data.get("first_name")
        return name.title()

    def clean_last_name(self):
        name = self.cleaned_data.get("last_name")
        return name.title()


class EditAdmForm(forms.ModelForm, BaseAddressForm):
    first_name = forms.CharField(widget=forms.TextInput(attrs={'required': True, 'maxlength': 200,
                                                               'placeholder': 'Nome',
                                                               }))
    last_name = forms.CharField(widget=forms.TextInput(attrs={'required': True, 'maxlength': 200,
                                                              'placeholder': 'Sobrenome',
                                                              }))
    email = forms.EmailField(widget=forms.EmailInput(attrs={'required': False, 'maxlength': 150,
                                                            'placeholder': 'Email',
                                                            }))
    phone = forms.CharField(widget=forms.TextInput(attrs={'required': False, 'maxlength': 16,
                                                             'placeholder': 'Telefone',
        }))

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email']

    def __init__(self, *args, **kwargs):
        super(EditAdmForm, self).__init__(*args, **kwargs)

    def clean_first_name(self):
        name = self.cleaned_data.get("first_name")
        return name.title()

    def clean_last_name(self):
        name = self.cleaned_data.get("last_name")
        return name.title()


class ProductForm(BaseForm):
    name = forms.CharField(widget=forms.TextInput(attrs={'required': True, 'maxlength': 200,
                                                         'placeholder': 'Nome do Produto',
                                                         }))
    brand = forms.CharField(widget=forms.TextInput(attrs={'required': True, 'maxlength': 200,
                                                          'placeholder': 'Marca do Produto',
                                                          }))

    distributor = forms.CharField(widget=forms.TextInput(attrs={'required': True, 'maxlength': 200,
                                                                'placeholder': 'Distribuidor do Produto',
                                                                }))

    cost = forms.FloatField(localize=True)

    profit = forms.FloatField(localize=True)

    stock = forms.IntegerField(localize=True)

    file = forms.FileField(required=False,
                           widget=forms.FileInput(attrs={'required': False, 'placeholder': 'Foto'
                                                         }))

    def __init__(self, *args, **kwargs):
        super(ProductForm, self).__init__(*args, **kwargs)
        self.fields['type'] = forms.ChoiceField(
            choices=[(o.id, str(o)) for o in ProductType.objects.all()], label='Tipo'
        )


class EditProductForm(forms.ModelForm):
    error_messages = {'type_not_found': _("Tipo de produto inválido.")}

    file = forms.FileField(required=False,
                           widget=forms.FileInput(attrs={'required': False, 'placeholder': 'Foto'
                                                         }))
    stock = forms.IntegerField(localize=True)

    class Meta:
        model = Product
        fields = ['name', 'brand', 'distributor', 'cost', 'profit', 'type', 'stock']

    def __init__(self, *args, **kwargs):
        super(EditProductForm, self).__init__(*args, **kwargs)
        self.fields['type'] = forms.ChoiceField(
            choices=[(o.id, str(o)) for o in ProductType.objects.all()], label='Tipo', required=False
        )

    def clean_type(self):
        type_id = self.cleaned_data.get("type")
        type = ProductType.objects.get(id=type_id)
        if (type == None):
            raise forms.ValidationError(
                self.error_messages['type_not_found'],
                code='type_not_found',
            )
        return type


class BasketSkelForm(BaseForm):
    name = forms.CharField(widget=forms.TextInput(attrs={'required': True, 'maxlength': 200,
                                                         'placeholder': 'Nome da Cesta',
                                                         }), label='Nome')

    def __init__(self, *args, **kwargs):
        super(BasketSkelForm, self).__init__(*args, **kwargs)
        for prodtype in ProductType.objects.all():
            if (prodtype.name != "Outros"):
                field_name = u'{0}'.format(prodtype.name)
                self.fields[field_name] = forms.CharField(widget=forms.TextInput(attrs={'required': False}), initial=0)


class AutoBasketForm(BaseForm):
    profit = forms.FloatField(localize=True)
    file = forms.FileField(required=False,
                           widget=forms.FileInput(attrs={'required': False, 'placeholder': 'Foto'
                                                         }), label="Foto")

    def __init__(self, *args, **kwargs):
        super(AutoBasketForm, self).__init__(*args, **kwargs)
        self.fields['basketskel'] = forms.ChoiceField(
            choices=[(o.id, str(o)) for o in BasketSkel.objects.all()]
        )


class BasketForm(forms.ModelForm):
    class Meta:
        model = Basket
        fields = ["name", "profit", "stock"]
        localized_fields = ["profit"]
        labels = {"name" : "Nome", "profit" : "Lucro", "stock" : "Estoque"}

    file = forms.FileField(required=False,
                           widget=forms.FileInput(attrs={'required': False, 'placeholder': 'Foto'
                                                         }), label="Foto")


class ProductTypeForm(BaseForm):
    name = forms.CharField(max_length=50)

    def __init__(self, *args, **kwargs):
        super(ProductTypeForm, self).__init__(*args, **kwargs)


class BasketContentForm(forms.ModelForm):
    class Meta:
        model = BasketContent
        fields = ["product", "qnt"]
        labels = {"product" : "Produto", "qnt": "Quantidade"}


class BaseBasketFormSet(forms.BaseInlineFormSet):
    def clean(self):
        if any(self.errors):
            return
        prods = []
        for form in [f for f in self.forms if  f.cleaned_data and not f.cleaned_data.get('DELETE', False)]:
            prod = form.cleaned_data['product']
            if prod in prods:
                raise forms.ValidationError("Não é possível adicionar duas vezes o mesmo produto.")
            prods.append(prod)


BasketContentFormSet = forms.inlineformset_factory(parent_model=Basket, model=BasketContent, form=BasketContentForm,
                                                   formset=BaseBasketFormSet, extra=1, can_delete=True)


class CartProductForm(forms.ModelForm):
    error_messages = {'stock_error': _("Não ha estoque suficiente.")}
    class Meta:
        model = CartProduct
        fields = ["qnt"]
        labels = {"qnt": "Quantidade"}

    def clean_qnt(self):
        estoque = self.instance.product.stock
        quantidade = self.cleaned_data.get("qnt")
        if estoque < quantidade:
            raise forms.ValidationError(
                self.error_messages['stock_error'],
                code='stock_error',
            )
        return quantidade


CartProductFormSet = forms.inlineformset_factory(parent_model=Cart, model=CartProduct, form=CartProductForm, extra=0,
                                                 can_delete=True)


class CartBasketForm(forms.ModelForm):
    error_messages = {'stock_error': _("Não ha estoque suficiente.")}
    class Meta:
        model = CartBasket
        fields = ["qnt"]
        labels = {"qnt": "Quantidade"}

    def clean_qnt(self):
        estoque = self.instance.basket.stock
        quantidade = self.cleaned_data.get("qnt")
        if estoque < quantidade:
            raise forms.ValidationError(
                self.error_messages['stock_error'],
                code='stock_error',
            )
        return quantidade


CartBasketFormSet = forms.inlineformset_factory(parent_model=Cart, model=CartBasket, form=CartBasketForm, extra=0,
                                                 can_delete=True)


class PurchaseForm(forms.ModelForm):
    class Meta:
        model = Purchase
        fields = ['status']

class RegisterPurchaseForm(BaseAddressForm):
    first_name = forms.CharField(widget=forms.TextInput(attrs={'required': True, 'maxlength': 200,
                                                               'placeholder': 'Nome',
                                                               }))
    last_name = forms.CharField(widget=forms.TextInput(attrs={'required': True, 'maxlength': 200,
                                                              'placeholder': 'Sobrenome',
                                                              }))
    phone = forms.CharField(widget=forms.TextInput(attrs={'required': False, 'maxlength': 16,
                                                             'placeholder': 'Telefone',
                                                             }))
    cpf = forms.CharField(widget=forms.TextInput(attrs={'required': True, 'maxlength': 150,
                                                        'placeholder': _('CPF'),
                                                        }))
    cep = forms.CharField(max_length=10, required=True, widget=forms.TextInput(attrs={
    }))

    def __init__(self, *args, **kwargs):
        super(RegisterPurchaseForm, self).__init__(*args, **kwargs)

    def clean_first_name(self):
        name = self.cleaned_data.get("first_name")
        return name.title()

    def clean_last_name(self):
        name = self.cleaned_data.get("last_name")
        return name.title()


class PurchaseBasketForm(forms.ModelForm):
    error_messages = {'stock_error': _("Não ha estoque suficiente.")}
    class Meta:
        model = PurchaseBasket
        fields = ["basket", "qnt"]
        labels = {"basket": "Cesta", "qnt": "Quantidade"}

    def clean_qnt(self):
        estoque = self.cleaned_data.get("basket").stock
        quantidade = self.cleaned_data.get("qnt")
        if estoque < quantidade:
            raise forms.ValidationError(
                self.error_messages['stock_error'],
                code='stock_error',
            )
        return quantidade


PurchaseBasketFormSet = forms.inlineformset_factory(parent_model=Purchase, model=PurchaseBasket,
                                                    form=PurchaseBasketForm, extra=0, can_delete=True)




class FreteForm(BaseForm):
    cep = forms.CharField(max_length=10, required=True, widget=forms.TextInput(attrs={
    }))
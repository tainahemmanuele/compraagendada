from django.contrib.auth.models import User
from django.db import models
from django.core.validators import MinValueValidator


# Create your models here.


class TimeStamped(models.Model):
    class Meta:
        abstract = True

    created_at = models.DateTimeField(auto_now_add=True)
    published_at = models.DateTimeField(auto_now=True)


class BaseAddress(models.Model):
    class Meta:
        abstract = True

    cep = models.CharField(max_length=15, blank=True)
    state = models.CharField(max_length=20, blank=True)
    city = models.CharField(max_length=100, blank=True)
    district = models.CharField(max_length=100, blank=True)
    address = models.CharField(max_length=100, blank=True)
    number = models.CharField(max_length=5, blank=True, null=True)
    complement = models.CharField(max_length=200, blank=True)


class Cart(models.Model):
    frete = models.FloatField(blank=True, default=0)

    class Meta:
        verbose_name = "Carrinho"
        verbose_name_plural = "Carrinhos"

    @property
    def total(self):
        acum = 0.0

        for cont in self.cartproduct_set.all():
            acum += cont.qnt * cont.product.price

        for cont in self.cartbasket_set.all():
            acum += cont.qnt * cont.basket.price

        if (self.frete != 0):
            acum += self.frete

        return float("%.2f" % acum)


class ServiceClient(TimeStamped, BaseAddress):
    class Meta:
        verbose_name = "Cliente"
        verbose_name_plural = "Clientes"

    user = models.OneToOneField(User, on_delete=models.CASCADE, unique=True)
    phone = models.CharField(max_length=30, blank=True)
    cpf = models.CharField(max_length=100, blank=True, unique=True)
    cart = models.OneToOneField(Cart, on_delete=models.CASCADE, unique=True, null=True)
    active = models.BooleanField(default=True)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self._state.adding is True:
            self.cart = Cart.objects.create()
        super(ServiceClient, self).save(force_insert=force_insert, force_update=force_update,
                                        using=using, update_fields=update_fields)

    def delete(self, using=None, keep_parents=False):
        if self.active:
            self.active = False
            self.user.set_unusable_password()
            self.user.username = self.cpf
            self.user.save()
            self.save()

    def __unicode__(self):
        return u'%s %s' % (self.user.first_name, self.user.last_name)

    def __str__(self):
        return '%s %s' % (self.user.first_name, self.user.last_name)


class ItemManager(models.Manager):
    def get_queryset(self):
        return super(ItemManager, self).get_queryset().filter(available=True)


class Item(models.Model):
    class Meta:
        abstract = True

    name = models.CharField(max_length=300)
    cost = models.FloatField(blank=True, default=0)
    profit = models.FloatField(blank=True, default=0)
    photo = models.URLField(blank=True, null=True)
    stock = models.IntegerField(default=0, validators=[MinValueValidator(0)])
    available = models.BooleanField(default=True)
    sold = models.IntegerField(default=0, validators=[MinValueValidator(0)])

    objects = ItemManager()
    all_objects = models.Manager()

    def delete(self, using=None, keep_parents=False):
        if self.available:
            self.available = False
            self.stock = 0
            self.save()

    @property
    def price(self):
        real_price = self.cost + (self.cost * self.profit)
        return float("%.2f" % real_price)

    @property
    def stock_state(self):
        if self.stock > 10:
            return "Em Estoque"
        elif self.stock == 0:
            return "Esgotado"
        else:
            return "Ultimas Unidades"


class ServiceAdm(TimeStamped):
    class Meta:
        verbose_name = "Administrador"
        verbose_name_plural = "Administradores"

    user = models.OneToOneField(User, on_delete=models.CASCADE, unique=True)
    phone = models.CharField(max_length=30, blank=True)

    def __unicode__(self):
        return u'%s' % self.user.first_name

    def __str__(self):
        return self.user.first_name


class ProductType(models.Model):
    class Meta:
        verbose_name = "Tipo de Produto"
        verbose_name_plural = "Tipos de Produto"
        ordering = ['name']

    name = models.CharField(max_length=50, unique=True)

    def __unicode__(self):
        return u'%s' % self.name

    def __str__(self):
        return self.name


class BasketSkel(TimeStamped):
    class Meta:
        verbose_name = "Estrutura de Cesta"
        verbose_name_plural = "Estruturas de Cesta"

    name = models.CharField(max_length=300)

    def __unicode__(self):
        return u'%s' % self.name

    def __str__(self):
        return self.name


class ContentQnt(models.Model):
    basketskel = models.ForeignKey(to=BasketSkel, on_delete=models.CASCADE)
    type = models.ForeignKey(ProductType, on_delete=models.PROTECT)
    qnt = models.IntegerField()

    def __unicode__(self):
        return u'x%d %s' % (self.qnt, str(self.type))

    def __str__(self):
        return 'x%d %s' % (self.qnt, str(self.type))


class Product(TimeStamped, Item):
    class Meta:
        verbose_name = "Produto"
        verbose_name_plural = "Produtos"

    brand = models.CharField(max_length=100)
    distributor = models.CharField(max_length=100)
    type = models.ForeignKey(ProductType, on_delete=models.PROTECT)

    def __unicode__(self):
        return u'%s %s - R$ %.2f' % (self.name, self.brand, self.cost)

    def __str__(self):
        return '%s %s - R$ %.2f' % (self.name, self.brand, self.cost)


class Basket(TimeStamped, Item):
    class Meta:
        verbose_name = "Cesta"
        verbose_name_plural = "Cestas"

    def __unicode__(self):
        return u'%s' % self.name

    def __str__(self):
        return self.name


class BasketContent(models.Model):
    basket = models.ForeignKey(to=Basket, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.PROTECT)
    qnt = models.IntegerField(validators=[MinValueValidator(1)])

    def __unicode__(self):
        return u'x%d %s' % (self.qnt, str(self.product))

    def __str__(self):
        return 'x%d %s' % (self.qnt, str(self.product))


class CartProduct(models.Model):
    cart = models.ForeignKey(to=Cart, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.PROTECT)
    qnt = models.IntegerField(default=1, validators=[MinValueValidator(1)])

    def __unicode__(self):
        return u'x%d %s' % (self.qnt, self.product.name)

    def __str__(self):
        return 'x%d %s' % (self.qnt, self.product.name)


class CartBasket(models.Model):
    cart = models.ForeignKey(to=Cart, on_delete=models.CASCADE)
    basket = models.ForeignKey(Basket, on_delete=models.PROTECT)
    qnt = models.IntegerField(default=1, validators=[MinValueValidator(1)])

    def __unicode__(self):
        return u'x%d %s' % (self.qnt, self.basket.name)

    def __str__(self):
        return 'x%d %s' % (self.qnt, self.basket.name)


purchase_status = (
    ("IN_PROGRESS", "Em Andamento"),
    ("AWAITING_PAYMENT", "Aguardando Pagamento"),
    ("COMPLETED", "Concluida"),
    ("CANCELED", "Cancelada"),
)


class Purchase(TimeStamped):
    class Meta:
        verbose_name = "Compra"
        verbose_name_plural = "Compras"

    client = models.ForeignKey(User, on_delete=models.CASCADE)
    status = models.CharField(max_length=20, choices=purchase_status, default="IN_PROGRESS")
    frete_value = models.FloatField(blank=True, default=0)

    @property
    def total(self):
        acum = 0.0

        for cont in self.purchaseproduct_set.all():
            acum += cont.qnt * cont.price

        for cont in self.purchasebasket_set.all():
            acum += cont.qnt * cont.price


        acum += self.client.serviceclient.cart.frete
        self.frete_value = self.client.serviceclient.cart.frete

        return float("%.2f" % acum)



class PurchaseProduct(models.Model):
    purchase = models.ForeignKey(to=Purchase, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.PROTECT)
    qnt = models.IntegerField(default=1, validators=[MinValueValidator(1)])
    price = models.FloatField(blank=True, default=0)

    def __unicode__(self):
        return u'x%d %s - %.2f' % (self.qnt, self.product.name, (self.qnt * self.price))

    def __str__(self):
        return 'x%d %s - %.2f' % (self.qnt, self.product.name, (self.qnt * self.price))

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.price = self.product.price
        super(PurchaseProduct, self).save(force_insert=force_insert, force_update=force_update,
                                          using=using, update_fields=update_fields)


class PurchaseBasket(models.Model):
    purchase = models.ForeignKey(to=Purchase, on_delete=models.CASCADE)
    basket = models.ForeignKey(Basket, on_delete=models.PROTECT)
    qnt = models.IntegerField(default=1, validators=[MinValueValidator(1)])
    price = models.FloatField(blank=True, default=0)

    def __unicode__(self):
        return u'x%d %s - %.2f' % (self.qnt, self.basket.name, (self.qnt * self.price))

    def __str__(self):
        return 'x%d %s - %.2f' % (self.qnt, self.basket.name, (self.qnt * self.price))

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.price = self.basket.price
        super(PurchaseBasket, self).save(force_insert=force_insert, force_update=force_update,
                                         using=using, update_fields=update_fields)
